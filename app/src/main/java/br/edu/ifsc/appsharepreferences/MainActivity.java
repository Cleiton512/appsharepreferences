package br.edu.ifsc.appsharepreferences;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    TextView txtResultado;
    EditText edtTexto;
    Button btnSalvar;

    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    private final String FILE= "ARQUIVO";
    private final String BLOCO_DE_NOTAS = "bloco";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtResultado = findViewById(R.id.txtResultado);
        edtTexto = findViewById(R.id.edtTexto);
        btnSalvar = findViewById(R.id.btnSalvar);

        preferences = getSharedPreferences(FILE, MODE_PRIVATE);
        editor = preferences.edit();

    }

    public void Salvar_e_Recuperar(View v){
        editor.putString(BLOCO_DE_NOTAS, edtTexto.getText().toString());
        editor.commit();
        txtResultado.setText(preferences.getString(BLOCO_DE_NOTAS,
                getResources().getString(R.string.texto_nao_encontrado)));
    }

    @Override
    protected void onStart() {
        super.onStart();
        txtResultado.setText(preferences.getString(BLOCO_DE_NOTAS,
                getResources().getString(R.string.texto_nao_encontrado)));
    }
}
